package com.sandbox;

import com.github.davidmoten.rx.Transformers;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.Nonnull;

import rx.Observable;

import static java.lang.System.out;

public class Main {

    public static void main(String[] args) {
        Patterner patterner = new StreamPatterner();

        // "Unit tests"
        try {
            patterner.patternAt("123a", 2);
            assert false;
        } catch (IllegalArgumentException e) {
            assert e.getLocalizedMessage().equals("pattern must contain only numbers! Was 123a");
        }

        assert "11".equals(patterner.patternAt("1", 2));
        assert "11".equals(patterner.patternAt("11", 1));
        assert "21".equals(patterner.patternAt("11", 2));
        assert "1211".equals(patterner.patternAt("11", 3));
        assert "111221".equals(patterner.patternAt("11", 4));
        assert "312211".equals(patterner.patternAt("11", 5));
        assert "13112221".equals(patterner.patternAt("11", 6));
        assert "1113213211".equals(patterner.patternAt("11", 7));

        String res;

        res = patterner.patternAt("1", 2);
        out.println("1, 2=" + res);

        res = patterner.patternAt("11", 1);
        out.println("11, 1=" + res);

        res = patterner.patternAt("11", 2);
        out.println("11, 2=" + res);

        res = patterner.patternAt("11", 3);
        out.println("11, 3=" + res);

        res = patterner.patternAt("11", 4);
        out.println("11, 4=" + res);

        res = patterner.patternAt("11", 5);
        out.println("11, 5=" + res);

        res = patterner.patternAt("11", 6);
        out.println("11, 6=" + res);

        res = patterner.patternAt("11", 7);
        out.println("11, 7=" + res);
    }

    interface Patterner {

        @Nonnull
        String patternAt(@Nonnull String start, int iteration);
    }

    static class UglyPatterner implements Patterner {

        @Override
        @Nonnull
        public String patternAt(@Nonnull String start, int iteration) {
            if (!start.matches("\\d+")) {
                throw new IllegalArgumentException("pattern must contain only numbers! Was " + start);
            }

            if (iteration == 1) {
                return start;
            }

            String next = "";
            String temp = "";
            for (int i = 0, n = start.length() - 1; i < n; i++) {
                String a = start.substring(i, i + 1);
                String b = start.substring(i + 1, i + 2);
                if (a.equals(b)) {
                    if (temp.length() == 0) {
                        temp += a + b;
                    } else {
                        temp += b;
                    }
                } else {
                    if (temp.length() > 0) {
                        next += String.valueOf(temp.length()) + a;
                        temp = "";
                        if (i == n - 1) {
                            temp = b;
                        }
                    } else {
                        next += "1" + a;
                        temp = b;
                    }
                }
            }
            if (temp.length() > 0) {
                next += String.valueOf(temp.length()) + temp.substring(0, 1);
            }

            return patternAt(next, iteration - 1);
        }
    }

    static class BetterPatterner implements Patterner {

        @Override
        @Nonnull
        public String patternAt(@Nonnull String start, int iteration) {
            if (!start.matches("\\d+")) {
                throw new IllegalArgumentException("pattern must contain only numbers! Was " + start);
            }

            if (iteration == 1) {
                return start;
            }

            List<StringBuilder> list = delimitPattern(start);
            String next = delimitedPatternToNextPattern(list);

            return patternAt(next, iteration - 1);
        }

        @Nonnull
        private static List<StringBuilder> delimitPattern(@Nonnull String start) {
            List<StringBuilder> list = new LinkedList<>();
            StringBuilder temp = new StringBuilder(Character.toString(start.charAt(0)));
            for (int i = 1, n = start.length(); i < n; i++) {
                char c = start.charAt(i);
                if (c == temp.charAt(0)) {
                    temp.append(c);
                } else {
                    list.add(temp);
                    temp = new StringBuilder(Character.toString(c));
                }
            }
            list.add(temp);
            return list;
        }

        @Nonnull
        private static String delimitedPatternToNextPattern(@Nonnull List<StringBuilder> list) {
            final StringBuilder next = new StringBuilder();
            list.forEach(s -> {
                next.append(s.length());
                next.append(s.charAt(0));
            });
            return next.toString();
        }
    }

    static class StreamPatterner implements Patterner {

        @Override
        @Nonnull
        public String patternAt(@Nonnull String start, int iteration) {
            if (!start.matches("\\d+")) {
                throw new IllegalArgumentException("pattern must contain only numbers! Was " + start);
            }

            if (iteration == 1) {
                return start;
            }

            char[] chars = start.toCharArray();
            StringBuilder sb = new StringBuilder();

            Observable.range(0, chars.length).map(i -> chars[i])
                    .compose(Transformers.<Character>toListWhile((list, t) ->
                            list.isEmpty() || list.get(0) == t))
                    .forEach(list -> {
                        sb.append(list.size());
                        sb.append(list.get(0));
                    });

            return patternAt(sb.toString(), iteration - 1);
        }
    }
}