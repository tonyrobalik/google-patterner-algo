# README #

This is an implementation of an algorithm described verbally to me by a friend who was asked to write it during a technical interview with Google. I thought it interesting, so I took a stab at it.

There are three implementations:

 1. UglyPatterner, which was the first version I got working and has a pretty ugly for loop at its core.
 2. BetterPatterner, which is really just an improvement on UglyPatterner (more readable).
 3. StreamPatterner, which uses RxJava and takes a stream-based approach. This is the version I _wanted_ to write first, but didn't know enough Rx to do initially.